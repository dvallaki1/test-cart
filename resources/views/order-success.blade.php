@extends('layouts.app')

@section('content')

<section class="h-100" style="background-color: #eee;">
  <div class="container h-100 py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-10">

        <div class="d-flex justify-content-between align-items-center mb-4">
          <h3 class="fw-normal mb-0 text-black">Order Details</h3>
        </div>


<h4>Thank you for placing order.. Your order details are as below:</h4>

<div class="card mb-4">
    <div class="card-body p-4 d-flex flex-row">
      <div class="col-md-12">
          <h5 class="mb-0">Order ID: {{$order->id}}</h5>
          <h5 class="mb-0">Name: {{@$order->user->name}}</h5>
          <h5 class="mb-0">Email: {{@$order->user->email}}</h5>
          <h5 class="mb-0">Address: {{$order->address}}</h5>
          <h5 class="mb-0">Order Total: ${{number_format($order->order_amount,2)}}</h5>
        </div>
    </div>
</div>

<div class="card rounded-3 mb-4">
  <div class="card-body p-4">
    <div class="row d-flex justify-content-between align-items-center">
      <div class="col-md-5">
        Product
      </div>
      <div class="col-md-2 d-flex">
        Quantity
      </div>
      <div class="col-md-2">
        Price
      </div>
      <div class="col-md-2">
        Subtotal
      </div>
    </div>
  </div>
</div>
@foreach($order->products as $product)
  <div class="card rounded-3 mb-4">
    <div class="card-body p-4">
      <div class="row d-flex justify-content-between align-items-center">
        <div class="col-md-2">
          <img
            src="{{@$product->product->image}}"
            class="img-fluid rounded-3" alt="{{@$product->product->name}}">
        </div>
        <div class="col-md-3">
          <p class="lead fw-normal mb-2">{{@$product->product->name}}</p>
        </div>
        <div class="col-md-2 d-flex">
          <h5 class="mb-0">{{$product->qty}}</h5>
        </div>
        <div class="col-md-2">
          <h5 class="mb-0">${{number_format($product->price/$product->qty,2)}}</h5>
        </div>
        <div class="col-md-2">
          <h5 class="mb-0">${{number_format($product->price,2)}}</h5>
        </div>
      </div>
    </div>
  </div>
@endforeach

@endsection