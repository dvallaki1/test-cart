@extends('layouts.app')

@section('content')

<section class="h-100" style="background-color: #eee;">
  <div class="container h-100 py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-10">

        <div class="d-flex justify-content-between align-items-center mb-4">
          <h3 class="fw-normal mb-0 text-black">Shopping Cart</h3>
        </div>

        @if(Session::has('success'))
            <p style="color: green">{{Session::get('success')}}</p>
        @endif
        @if(Session::has('error'))
            <p style="color: red">{{Session::get('error')}}</p>
        @endif

        <div id="cart-div">
          @if(count($cart_lists) > 0)
          <div class="card rounded-3 mb-4">
            <div class="card-body p-4">
              <div class="row d-flex justify-content-between align-items-center">
                <div class="col-md-5">
                  Product
                </div>
                <div class="col-md-2 d-flex">
                  Quantity
                </div>
                <div class="col-md-2">
                  Price
                </div>
                <div class="col-md-2">
                  Subtotal
                </div>
                <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                  Action
                </div>
              </div>
            </div>
          </div>
          @foreach($cart_lists as $cart)
          <div class="card rounded-3 mb-4">
            <div class="card-body p-4">
              <div class="row d-flex justify-content-between align-items-center">
                <div class="col-md-2">
                  <img
                    src="{{$cart->options->image}}"
                    class="img-fluid rounded-3" alt="{{$cart->name}}">
                </div>
                <div class="col-md-3">
                  <p class="lead fw-normal mb-2">{{$cart->name}}</p>
                </div>
                <div class="col-md-2 d-flex">
                  <button class="btn btn-link px-2"
                    onclick="this.parentNode.querySelector('input[type=number]').stepDown();test('{{$cart->rowId}}',this.parentNode.querySelector('input[type=number]').value)">
                    <i class="fas fa-minus"></i>
                  </button>

                  <input min="0" name="quantity" value="{{$cart->qty}}" type="number"
                    class="form-control form-control-sm changeQty" data-id="{{$cart->rowId}}" readonly="" />

                  <button class="btn btn-link px-2"
                    onclick="this.parentNode.querySelector('input[type=number]').stepUp();test('{{$cart->rowId}}',this.parentNode.querySelector('input[type=number]').value)">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
                <div class="col-md-2">
                  <h5 class="mb-0">${{number_format($cart->price,2)}}</h5>
                </div>
                <div class="col-md-2">
                  <h5 class="mb-0">${{number_format($cart->price*$cart->qty,2)}}</h5>
                </div>
                <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                  <a href="{{ url('delete-from-cart/'.$cart->rowId) }}" class="text-danger"><i class="fas fa-trash fa-lg"></i></a>
                </div>
              </div>
            </div>
          </div>
          @endforeach

          <div class="card mb-4">
            <div class="card-body p-4 d-flex flex-row">
              <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-8">
                  <h5 class="mb-0">Total: ${{number_format($grand_total,2)}}</h5>
                </div>
            </div>
          </div>

          <form method="post">
            @csrf
            <div class="card mb-4">
              <div class="card-body p-4 d-flex flex-row">
                <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-2">
                  <input type="hidden" name="user_id" value="{{@Auth::id()}}">
                  <input type="hidden" name="order_amount" value="{{$grand_total}}">
                  <label>Name</label>
                  <input type="text" name="name" value="{{@Auth::user()->name}}" required="">
                  <label>Email</label>
                  <input type="email" name="email" value="{{@Auth::user()->email}}" required="">
                  <label>Address</label>
                  <textarea name="address" rows="4" cols="70" required></textarea>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-body">
                  <button type="submit" class="btn btn-warning btn-block btn-lg">Place Order</button>
              </div>
            </div>

          </form>

          @else

          <div class="card">
            <div class="card-body">
              <h5 class="mb-0">Cart is empty</h5>
              <a href="{{url('/')}}">Go to products page</a>
            </div>
          </div>

          @endif
        </div>

      </div>
    </div>
  </div>
</section>


<script>

  function test(rowId, qty)
  {
    // var id = $(this).data('id');
    // var qty = $(this).val();
    $.ajax({
        type: 'post',
        url: "{{url('update-cart')}}",
        data: {
            qty: qty, rowId: rowId,
            "_token": "{{ csrf_token() }}",
        },
        success: function (data) {
          if (data.success == true) {
              $("#cart-div").load(location.href + " #cart-div");
          }
        }
    });
  }

</script>

@endsection