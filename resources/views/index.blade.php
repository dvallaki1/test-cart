@extends('layouts.app')

@section('content')

<h2 class="text-center">Product list</h2>
@if(count($products) > 0)
<div class="row">
@foreach($products as $key => $product)                          
    <div class="col-md-4">
        <div class="product-card">
            <img src="{{$product->image}}" alt="{{$product->name}}" style="width:100%">
            <h4>{{$product->name}}</h4>
            <p class="price">${{number_format($product->price,2)}}</p>
            <p><a class="btn btn-primary" href="{{url('add-to-cart')}}/{{$product->id}}">Add to Cart</a></p>
        </div>
    </div>
@endforeach
</div>
@else
No products found..
@endif

<div class="container">
@if ($products->hasPages())
<div class="pagination">
    {{-- First Page Link --}}
    @if($products->currentPage() > 1)
        <a href="{{ $products->url(1) }}"> << </a>
    @else
        <a> << </a>
    @endif
    {{-- Previous Page Link --}}
    @if ($products->onFirstPage())
        <a> < </a>
    @else
        <a  href="{{ $products->previousPageUrl() }}" rel="prev"> < </a>
    @endif
    
    @foreach(range(1, $products->lastPage()) as $i)
        @if($i >= $products->currentPage() - 1 && $i <= $products->currentPage() + 1 || (($products->onFirstPage() || !$products->hasMorePages()) && $i >= $products->currentPage() - 2 && $i <= $products->currentPage() + 2) )
            @if ($i == $products->currentPage())
                <a class="active">{{ $i }}</a>
            @else
                <a href="{{ $products->url($i) }}">{{ $i }}</a>
            @endif
        @endif
    @endforeach
    
    {{-- Next Page Link --}}
    @if ($products->hasMorePages())
        <a  href="{{ $products->nextPageUrl() }}" rel="next"> > </a>
    @else
        <a> > </a>
    @endif
    {{-- Last Page Link --}}
    @if($products->currentPage() < $products->lastPage())
        <a href="{{ $products->url($products->lastPage()) }}"> >> </a>
    @else
        <a> >> </a>
    @endif
</div>
@endif
</div>

@endsection