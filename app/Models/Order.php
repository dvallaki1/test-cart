<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','order_amount','address'];

    public function products(){
        return $this->hasMany(OrderProduct::class, 'order_id','id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
