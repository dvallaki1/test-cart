<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderProduct;
use Cart;

class OrderController extends Controller
{
    public function placeOrder(Request $Request)
    {
        $cart_lists = Cart::content();

        if(count($cart_lists) <= 0)
            return back()->with('error','Please add atleast one product to the cart');

    	$formData = request()->except(['_token']);

    	if($formData['user_id']=='')
    	{
    		$user = User::where('email',$formData['email'])->first();

    		if($user=='')
    		{
    			$user = User::create([
    								'name'=>$formData['name'],
    								'email'=>$formData['email'],
    							]);
    		}

            $formData['user_id'] = $user->id;
    	}
    	
    	$order = Order::create($formData);

    	foreach ($cart_lists as $key => $value) {
    		$orderProduct = OrderProduct::create([
    			'order_id' => $order->id,
    			'product_id' => $value->id,
    			'price' => $value->price*$value->qty,
    			'qty' => $value->qty
    		]);
    	}

    	Cart::destroy();

    	return view('order-success',compact('order'));
    }
}
