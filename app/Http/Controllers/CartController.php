<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Cart;

class CartController extends Controller
{
    public function addToCart($id, Request $Request)
    {
    	$product = Product::find($id);

    	$data['id'] = $id;
        $data['qty'] = 1;
        $data['price'] = $product->price;
        $data['name'] = $product->name;
        $data['options']['image'] = $product->image;
        $data['weight'] = 1;

        Cart::add($data);

        return redirect('cart')->with('success', 'Product added to cart successfully');
    }

    public function getCart($value='')
    {
    	$cart_lists = Cart::content();

        $grand_total = 0;

        foreach ($cart_lists as $key => $value) {
            $grand_total += $value->qty*$value->price;
        }

        return view('my-cart', compact('cart_lists','grand_total'));
    }

    public function deleteFromCart($id)
    {
        Cart::update($id, 0);
        return redirect('cart');
    }

    public function updateCart(Request $request)
    {
        Cart::update($request->rowId, $request->qty);
        return response()->json(['success'=>true]);
    }

    public function clearCart()
    {
        Cart::destroy();
        return redirect('cart')->with('success','You have successfully cleared cart');
    }
}
