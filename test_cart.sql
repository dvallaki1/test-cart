-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2023 at 03:04 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_cart`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_08_04_081559_create_products_table', 1),
(6, '2023_08_04_084444_create_orders_table', 1),
(7, '2023_08_04_084639_create_order_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `address` text DEFAULT NULL,
  `order_amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `address`, `order_amount`, `created_at`, `updated_at`) VALUES
(1, 1, 'sasd s dsd s fsd fdsf d fdf dsf d fd sasd s dsd s fsd fdsf d fdf dsf d fd sasd s dsd s fsd fdsf d fdf dsf d fd sasd s dsd s fsd fdsf d fdf dsf d fd', 400.00, '2023-08-04 06:11:33', '2023-08-04 06:11:33'),
(3, 1, 'sdsf fd dfg fg gf g f sdsf fd dfg fg gf g f sdsf fd dfg fg gf g f sdsf fd dfg fg gf g f sdsf fd dfg fg gf g f', 860.00, '2023-08-04 06:13:39', '2023-08-04 06:13:39'),
(4, 3, 'a asd sd f', 100.00, '2023-08-04 06:18:01', '2023-08-04 06:18:01'),
(5, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:19:08', '2023-08-04 06:19:08'),
(6, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:19:11', '2023-08-04 06:19:11'),
(7, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:20:04', '2023-08-04 06:20:04'),
(8, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:22:22', '2023-08-04 06:22:22'),
(9, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:22:50', '2023-08-04 06:22:50'),
(10, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:23:14', '2023-08-04 06:23:14'),
(11, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:23:25', '2023-08-04 06:23:25'),
(12, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:23:47', '2023-08-04 06:23:47'),
(13, 4, 'a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , a sa desdfrdf dfg , \r\n xsf ds fd fd gfdg', 1776.00, '2023-08-04 06:25:05', '2023-08-04 06:25:05'),
(14, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 06:27:08', '2023-08-04 06:27:08'),
(15, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 06:59:17', '2023-08-04 06:59:17'),
(16, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:00:07', '2023-08-04 07:00:07'),
(17, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:00:38', '2023-08-04 07:00:38'),
(18, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:01:08', '2023-08-04 07:01:08'),
(19, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:02:37', '2023-08-04 07:02:37'),
(20, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:03:10', '2023-08-04 07:03:10'),
(21, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:03:20', '2023-08-04 07:03:20'),
(22, 5, 'sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd sd sf,d s df d fd , de d fd', 1776.00, '2023-08-04 07:03:53', '2023-08-04 07:03:53'),
(23, 1, 'x d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrgx d efd d fd gffrg', 2373.00, '2023-08-04 07:34:16', '2023-08-04 07:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `price`, `qty`, `order_status`, `created_at`, `updated_at`) VALUES
(2, 3, 5, '300', '3', 'pending', '2023-08-04 06:13:40', '2023-08-04 06:13:40'),
(3, 3, 9, '560', '4', 'pending', '2023-08-04 06:13:40', '2023-08-04 06:13:40'),
(4, 4, 5, '100', '1', 'pending', '2023-08-04 06:18:01', '2023-08-04 06:18:01'),
(5, 5, 6, '1776', '2', 'pending', '2023-08-04 06:19:08', '2023-08-04 06:19:08'),
(6, 6, 6, '1776', '2', 'pending', '2023-08-04 06:19:11', '2023-08-04 06:19:11'),
(7, 7, 6, '1776', '2', 'pending', '2023-08-04 06:20:04', '2023-08-04 06:20:04'),
(8, 8, 6, '1776', '2', 'pending', '2023-08-04 06:22:22', '2023-08-04 06:22:22'),
(9, 9, 6, '1776', '2', 'pending', '2023-08-04 06:22:50', '2023-08-04 06:22:50'),
(10, 10, 6, '1776', '2', 'pending', '2023-08-04 06:23:14', '2023-08-04 06:23:14'),
(11, 11, 6, '1776', '2', 'pending', '2023-08-04 06:23:25', '2023-08-04 06:23:25'),
(12, 12, 6, '1776', '2', 'pending', '2023-08-04 06:23:47', '2023-08-04 06:23:47'),
(13, 13, 6, '1776', '2', 'pending', '2023-08-04 06:25:05', '2023-08-04 06:25:05'),
(14, 14, 6, '1776', '2', 'pending', '2023-08-04 06:27:08', '2023-08-04 06:27:08'),
(15, 15, 6, '1776', '2', 'pending', '2023-08-04 06:59:17', '2023-08-04 06:59:17'),
(16, 16, 6, '1776', '2', 'pending', '2023-08-04 07:00:07', '2023-08-04 07:00:07'),
(17, 17, 6, '1776', '2', 'pending', '2023-08-04 07:00:38', '2023-08-04 07:00:38'),
(18, 18, 6, '1776', '2', 'pending', '2023-08-04 07:01:08', '2023-08-04 07:01:08'),
(19, 19, 6, '1776', '2', 'pending', '2023-08-04 07:02:37', '2023-08-04 07:02:37'),
(20, 20, 6, '1776', '2', 'pending', '2023-08-04 07:03:10', '2023-08-04 07:03:10'),
(21, 21, 6, '1776', '2', 'pending', '2023-08-04 07:03:20', '2023-08-04 07:03:20'),
(22, 22, 6, '1776', '2', 'pending', '2023-08-04 07:03:53', '2023-08-04 07:03:53'),
(23, 23, 6, '1776', '2', 'pending', '2023-08-04 07:34:16', '2023-08-04 07:34:16'),
(24, 23, 3, '597', '1', 'pending', '2023-08-04 07:34:16', '2023-08-04 07:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Temporibus nihil nihil dolor consectetur.', 'https://via.placeholder.com/640x480.png/00bb99?text=qui', 'Accusamus illo rerum in reprehenderit. Occaecati qui a totam sed illum sapiente corporis. Quisquam velit aliquam officiis natus in minima.', 665.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(2, 'Molestiae soluta corrupti non est.', 'https://via.placeholder.com/640x480.png/0055ff?text=beatae', 'Vero sed omnis ducimus corrupti maiores ratione animi mollitia. Voluptatem sint corrupti nesciunt quia iste natus magni.', 781.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(3, 'Ut qui et beatae earum ea.', 'https://via.placeholder.com/640x480.png/0066ee?text=id', 'Ut omnis adipisci distinctio id deserunt iure. Quo explicabo consequuntur consequatur est rem maxime. Assumenda aut est sunt assumenda.', 597.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(4, 'Provident et molestiae quidem fuga asperiores qui et.', 'https://via.placeholder.com/640x480.png/007700?text=in', 'Labore sed optio repellat rerum commodi voluptates. Quasi possimus quidem id. Repudiandae quidem corporis sint qui libero quia. Expedita molestiae hic explicabo recusandae deleniti quo.', 531.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(5, 'Eaque eum tempore dolor numquam enim deserunt.', 'https://via.placeholder.com/640x480.png/00ee00?text=aperiam', 'Totam officiis voluptatem vel aperiam sit dolor. Aut ut vel fugit ex iusto officiis.', 100.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(6, 'Fugit inventore magni enim.', 'https://via.placeholder.com/640x480.png/008833?text=ratione', 'Accusamus sunt odit distinctio neque atque rerum. Culpa aliquid enim id deserunt et. Et sed corporis distinctio quia inventore qui.', 888.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(7, 'Eos suscipit quis ipsa atque ut delectus.', 'https://via.placeholder.com/640x480.png/00eebb?text=deserunt', 'Enim temporibus alias est. Facilis magnam est fugiat sed esse. Iusto consequatur reiciendis vel quia dolores eum animi eum.', 532.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(8, 'Quisquam impedit natus quis sunt nobis.', 'https://via.placeholder.com/640x480.png/007700?text=omnis', 'Consectetur voluptates voluptatem earum consequatur molestiae esse. Nam nisi quam nulla tempore eos. Distinctio ad non eaque ipsa qui dolorem ut.', 778.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(9, 'Ullam aut ducimus quia non.', 'https://via.placeholder.com/640x480.png/0022ff?text=cupiditate', 'Ullam dolore reprehenderit possimus voluptatem odit id. Aut esse animi sequi eveniet aspernatur reiciendis voluptates. Animi fuga ut consequatur animi.', 140.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36'),
(10, 'Voluptate sequi qui quod nesciunt.', 'https://via.placeholder.com/640x480.png/00ff77?text=numquam', 'Consequuntur sint consequatur quaerat optio voluptatem qui. Modi minus excepturi deleniti expedita et.', 385.00, '2023-08-04 03:26:36', '2023-08-04 03:26:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'vallaki dudhiya', 'dvallaki@gmail.com', NULL, '$2y$10$Hwl.aGHU/PQw90B7/5JHceA/KeEbkLMwpKceClcf6mZhfm/xOj5A2', 'cosOAiGp2iSHON6f5QDNCuBfdoyDN8Bck15Rog8xMZnvOgW2ZfP32n0D4OKe', '2023-08-04 06:11:33', '2023-08-04 07:33:24'),
(3, 'vallaki dudhiya', 'test@gmail.com', NULL, NULL, NULL, '2023-08-04 06:18:01', '2023-08-04 06:18:01'),
(4, 'vallaki dudhiya', 'dvllaki@gail.com', NULL, NULL, NULL, '2023-08-04 06:19:08', '2023-08-04 06:19:08'),
(5, 'test abc', 'test@aaaa.sds', NULL, NULL, NULL, '2023-08-04 06:27:08', '2023-08-04 06:27:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_order_id_foreign` (`order_id`),
  ADD KEY `order_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
