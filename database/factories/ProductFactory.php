<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(),
            'image' => $this->faker->imageUrl(640,480),
            'price' => $this->faker->numberBetween($min = 100, $max = 1000),
            'description' => $this->faker->text(),
        ];
    }
}
